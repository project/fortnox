/**
 * @file
 * Update currency values.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.fortnoxCurrencies = {
    attach: function (context, settings) {
      var $currencySelectList = $('[name="Currency"]');
      var $currencyUnit = $('[name="CurrencyUnit"]');
      var $currencyRate = $('[name="CurrencyRate"]');
      var currencyValue = $currencySelectList.val();
      $currencySelectList.on('change', function () {
        currencyValue = $(this).val();
        $currencyUnit.val(drupalSettings[currencyValue]['CurrencyUnit']);
        $currencyRate.val(drupalSettings[currencyValue]['CurrencyRate']);
      });

      // Remove Debit field value when the Credit field has value and
      // vice versa.
      var $debit = $('[name^="SupplierInvoice"][name*="[Debit]"]');
      var $credit = $('[name^="SupplierInvoice"][name*="[Credit]"]');
      $debit.on('input', function () {
        $(this).closest('div').prev().find('input').val('');
      });
      $credit.on('input', function () {
        $(this).closest('div').next().find('input').val('');
      })
    }
  };

})(jQuery, Drupal, drupalSettings);
