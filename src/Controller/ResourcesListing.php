<?php

namespace Drupal\fortnox\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\fortnox\Services\FortnoxClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ResourcesListing.
 *
 * Lists resources defined in Resource plugins.
 */
class ResourcesListing extends ControllerBase {

  /**
   * The fortnox client.
   *
   * @var \Drupal\fortnox\Services\FortnoxClientInterface
   */
  public $fortnoxClient;

  /**
   * ResourcesListing constructor.
   *
   * @param \Drupal\fortnox\Services\FortnoxClientInterface $fortnox_client
   *   The fortnox client.
   */
  public function __construct(FortnoxClientInterface $fortnox_client) {
    $this->fortnoxClient = $fortnox_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('fortnox.client')
    );
  }

  /**
   * Renders the list of resources defined by a plugin.
   *
   * @param mixed $resource
   *   The id of the resource.
   *
   * @return array
   *   Returns a renderable array containing a table and a pager.
   */
  public function renderResources($resource) {
    $build = [];

    if (is_object($resource)) {
      $build = $resource->getTableListing();
    }

    return $build;
  }

}
