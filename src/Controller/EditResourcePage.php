<?php

namespace Drupal\fortnox\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for edit resource page.
 */
class EditResourcePage extends ControllerBase {

  /**
   * Current route match property.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * EditResourcePage constructor.
   *
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   Current route match.
   */
  public function __construct(CurrentRouteMatch $currentRouteMatch) {
    $this->routeMatch = $currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match')
    );
  }

  /**
   * Creates the page elements.
   *
   * @return array
   *   The page elements array.
   */
  public function build() {
    $resource = $this->routeMatch->getParameter('resource');
    $build = [
      'form' => $resource->getCreateForm(),
    ];

    return $build;
  }

}
