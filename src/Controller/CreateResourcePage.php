<?php

namespace Drupal\fortnox\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class CreateResourcePage.
 */
class CreateResourcePage extends ControllerBase {

  /**
   * Gets the crete form for the current resource.
   *
   * @param mixed $resource
   *   The ID of the resource.
   *
   * @return array
   *   The form for the current resource.
   */
  public function getCreateResourceForm($resource) {
    if (is_object($resource)) {
      return $resource->getCreateForm();
    }

    return [];
  }

}
