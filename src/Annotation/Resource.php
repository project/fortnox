<?php

namespace Drupal\fortnox\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Resource item annotation object.
 *
 * @see \Drupal\fortnox\Plugin\ResourceManager
 * @see plugin_api
 *
 * @Annotation
 */
class Resource extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
