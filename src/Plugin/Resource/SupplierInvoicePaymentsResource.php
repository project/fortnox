<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox supplier payments resources.
 *
 * @Resource(
 *   id = "supplier-invoice-payments",
 *   label = @Translation("Supplier Invoice Payments Resource")
 * )
 */
class SupplierInvoicePaymentsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'SupplierInvoicePayments';

  /**
   * {@inheritdoc}
   */
  protected $url = 'supplierinvoicepayments';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'SupplierInvoicePayment';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Number';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    $links = parent::getLinks($resourceId, $param1, $param2);
    $links[] = [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      '@url',
      'Booked',
      'Currency',
      'CurrencyUnit',
      'InvoiceDueDate',
      'InvoiceSupplierName',
      'InvoiceSupplierNumber',
      'Number',
      'Source',
      'VoucerNumber',
      'VoucherSeries',
      'VoucherSeries',
      'VoucherYear',
    ];
  }

}
