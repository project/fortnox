<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox currencies resources.
 *
 * @Resource(
 *   id = "currencies",
 *   label = @Translation("Currencies Resource")
 * )
 */
class CurrenciesResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Code';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Currency';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Currencies';

  /**
   * {@inheritdoc}
   */
  protected $url = 'currencies';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    $links = parent::getLinks($resourceId, $param1, $param2);
    $links[] = [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    $fields = parent::getDisabledFields();
    $resourceFields = [
      'Code',
      'Date',
      'IsAutomatic',
    ];
    $mergedFields = array_merge($fields, $resourceFields);

    return $mergedFields;
  }

}
