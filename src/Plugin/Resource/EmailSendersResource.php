<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox email senders resources.
 *
 * @Resource(
 *   id = "email-senders",
 *   label = @Translation("Trusted Email Senders")
 * )
 */
class EmailSendersResource extends ArchiveResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'EmailSenders';

  /**
   * {@inheritdoc}
   */
  protected $url = 'emailsenders';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Id';

}
