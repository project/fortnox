<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\fortnox\Form\ContractsForm;

/**
 * Defines a plugin used to interact with fortnox contracts resources.
 *
 * @Resource(
 *   id = "contracts",
 *   label = @Translation("Contracts Resource")
 * )
 */
class ContractsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Contracts';

  /**
   * {@inheritdoc}
   */
  protected $url = 'contracts';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Contract';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'DocumentNumber';

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      '@url',
      'BalanceCarriedForward',
      'Year',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreateForm() {
    return $this->formBuilder->getForm(ContractsForm::class);
  }

}
