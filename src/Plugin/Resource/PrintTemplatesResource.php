<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox print templates resources.
 *
 * @Resource(
 *   id = "print-templates",
 *   label = @Translation("Print Templates Resource")
 * )
 */
class PrintTemplatesResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'PrintTemplates';

  /**
   * {@inheritdoc}
   */
  protected $url = 'printtemplates';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = '';

}
