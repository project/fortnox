<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox orders resources.
 *
 * @Resource(
 *   id = "orders",
 *   label = @Translation("Orders Resource")
 * )
 */
class OrdersResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Orders';

  /**
   * {@inheritdoc}
   */
  protected $url = 'orders';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Order';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'DocumentNumber';

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    $fields = parent::getDisabledFields();
    $resourceFields = [
      '@urlTaxReductionList',
      'AdministrationFeeVAT',
      'BasisTaxReduction',
      'Cancelled',
      'ContributionPercent',
      'ContributionValue',
      'FreightVAT',
      'Gross',
      'HouseWork',
      'InvoiceReference',
      'Net',
      'OrderType',
      'OrganisationNumber',
      'RoundOff',
      'Sent',
      'TaxReduction',
      'Total',
      'TotalVat',
    ];
    $mergedFields = array_merge($fields, $resourceFields);

    return $mergedFields;
  }

}
