<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox asset types resources.
 *
 * @Resource(
 *   id = "asset-types",
 *   label = @Translation("Asset Types Resource")
 * )
 */
class AssetTypesResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Types';

  /**
   * {@inheritdoc}
   */
  protected $url = 'assets/types';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Types';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Id';

  /**
   * {@inheritdoc}
   */
  public function getMetaInformation(array &$response) {
    $meta = [];
    if (isset($response[$this->resourceIDPlural][0]['MetaInformation']['@TotalResources'])) {
      $meta = $response[$this->resourceIDPlural][0]['MetaInformation']['@TotalResources'];
      // Remove the MetaInformation from the response.
      unset($response[$this->resourceIDPlural][0]);
    }

    return $meta;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponse(array &$build, $id = NULL) {
    $response = parent::getResponse($build);

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    $links = parent::getLinks($resourceId, $param1, $param2);
    $links[] = [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      '@url',
      'Id',
      'InUse',
      'AccountAsset',
      'AccountValueLoss',
      'AccountSaleLoss',
      'AccountSaleWin',
      'AccountRevaluation',
      'AccountWriteDownAck',
      'AccountWriteDown',
      'AccountDepreciation',
    ];
  }

}
