<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox contract templates resources.
 *
 * @Resource(
 *   id = "contract-templates",
 *   label = @Translation("Contract Templates Resource")
 * )
 */
class ContractTemplatesResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'ContractTemplatess';

  /**
   * {@inheritdoc}
   */
  protected $url = 'contracttemplates';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'ContractTemplate';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'TemplateNumber';

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      '@url',
    ];
  }

}
