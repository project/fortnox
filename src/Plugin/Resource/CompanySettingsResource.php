<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox company settings resource.
 *
 * @Resource(
 *   id = "company-settings",
 *   label = @Translation("Company Settings Resource")
 * )
 */
class CompanySettingsResource extends CompanyInformationResource {

  /**
   * {@inheritdoc}
   */
  protected $url = 'settings/company';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'CompanySettings';

}
