<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;
use Drupal\fortnox\Form\ProjectForm;

/**
 * Defines a plugin used to interact with fortnox projects resources.
 *
 * @Resource(
 *   id = "projects",
 *   label = @Translation("Projects Resource")
 * )
 */
class ProjectsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Projects';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Project';

  /**
   * {@inheritdoc}
   */
  protected $url = 'projects';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'ProjectNumber';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    $links = parent::getLinks($resourceId, $param1 = '', $param2 = '');
    $links[] = [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreateForm() {
    return $this->formBuilder->getForm(ProjectForm::class);
  }

}
