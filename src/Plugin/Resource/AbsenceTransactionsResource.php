<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;
use Drupal\fortnox\Form\AbsenceTransactionsForm;

/**
 * Defines a plugin used to interact with fortnox transactions resources.
 *
 * @Resource(
 *   id = "absence-transactions",
 *   label = @Translation("Absence Transactions Resource")
 * )
 */
class AbsenceTransactionsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'AbsenceTransactions';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'AbsenceTransaction';

  /**
   * {@inheritdoc}
   */
  protected $url = 'absencetransactions';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'EmployeeId';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    $links = parent::getLinks($resourceId, $param1, $param2);
    $links[] = [
      'url' => Url::fromRoute('fortnox.delete_resource', [
        'resource' => $this->getPluginId(),
        'id' => $resourceId,
        'param1' => $param1,
        'param2' => $param2,
      ]),
      'title' => $this->t('Delete'),
    ];

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreateForm() {
    return $this->formBuilder->getForm(AbsenceTransactionsForm::class);
  }

}
