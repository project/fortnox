<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\fortnox\Form\AccountsForm;

/**
 * Defines a plugin used to interact with fortnox account resources.
 *
 * @Resource(
 *   id = "accounts",
 *   label = @Translation("Account Resource")
 * )
 */
class AccountsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Accounts';

  /**
   * {@inheritdoc}
   */
  protected $url = 'accounts';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Account';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Number';

  /**
   * {@inheritdoc}
   */
  public function getCreateForm() {
    return $this->formBuilder->getForm(AccountsForm::class);
  }

}
