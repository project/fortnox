<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox suppliers file resources.
 *
 * @Resource(
 *   id = "supplier-invoice-file-connections",
 *   label = @Translation("Supplier Invoice File Connections Resource")
 * )
 */
class SupplierInvoiceFileConnectionsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'SupplierInvoiceFileConnections';

  /**
   * {@inheritdoc}
   */
  protected $url = 'supplierinvoicefileconnections';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'SupplierInvoiceConnection';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'FileId';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    return [
      [
        'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
        'title' => $this->t('Delete'),
      ],
    ];
  }

}
