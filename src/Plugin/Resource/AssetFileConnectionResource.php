<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox asset connection resources.
 *
 * @Resource(
 *   id = "asset-file-connections",
 *   label = @Translation("Asset File Connection Resource")
 * )
 */
class AssetFileConnectionResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'AssetFileConnections';

  /**
   * {@inheritdoc}
   */
  protected $url = 'assetfileconnections';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'AssetFileConnection';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'AssetId';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    return [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];
  }

}
