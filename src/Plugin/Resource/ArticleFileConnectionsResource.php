<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox article connection resources.
 *
 * @Resource(
 *   id = "article-file-connections",
 *   label = @Translation("Article File Connections Resource")
 * )
 */
class ArticleFileConnectionsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'ArticleFileConnections';

  /**
   * {@inheritdoc}
   */
  protected $url = 'articlefileconnections';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'ArticleFileConnection';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'ArticleNumber';

  /**
   * Creates the resource operation links.
   *
   * @param int $resourceId
   *   The resource id from fortnox.
   * @param string $param1
   *   Optional parameter.
   * @param string $param2
   *   Optional parameter.
   *
   * @return array
   *   The operation links.
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    return [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];
  }

}
