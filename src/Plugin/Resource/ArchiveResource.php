<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Url;
use Drupal\fortnox\Plugin\ResourceBase;

/**
 * Defines a plugin used to interact with fortnox archive resources.
 *
 * @Resource(
 *   id = "archive",
 *   label = @Translation("Archive Resource")
 * )
 */
class ArchiveResource extends ResourceBase {

  /**
   * {@inheritdoc}
   */
  protected $url = 'archive';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Folder';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Folder';

  /**
   * {@inheritdoc}
   */
  public function addTableValues(array $values, array &$build) {
    if (!empty($values[$this->resourceIDPlural])) {
      foreach ($values[$this->resourceIDPlural] as $key => $resource) {
        // If the value is an array, we create a markup providing the key name
        // and a table where the values will be added.
        if (is_array($resource)) {
          $build[$key] = [
            'markup' => [
              '#markup' => $key,
            ],
            'table' => [
              '#theme' => 'table',
              '#header' => '',
            ],
          ];
          if (!empty($resource)) {
            $build[$key]['table']['#header'] = array_keys(reset($resource));
            $build[$key]['table']['#header'][] = $this->t('Operations');
            foreach ($resource as $values) {
              // Add the operations in each row.
              $values[] = $this->createOperations($values['Id']);
              $build[$key]['table']['#rows'][] = array_values($values);
            }
          }
          else {
            $build[$key]['table'] = [
              '#markup' => $this->t('We did not find any @type resources.', ['@type' => $key]),
              '#prefix' => '<div>',
              '#suffix' => '</div>',
            ];
          }
        }
        // If the values are strings, we do not need to put them inside the
        // values table.
        else {
          $build[$key] = [
            '#markup' => $key . ': ' . $resource . '<br>',
          ];
        }
      }
    }
  }

  /**
   * Creates the resource operation links.
   *
   * @param int $resourceId
   *   The resource id from fortnox.
   * @param string $param1
   *   Optional parameter.
   * @param string $param2
   *   Optional parameter.
   *
   * @return array
   *   The operation links.
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    return [
      [
        'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
        'title' => $this->t('Delete'),
      ],
    ];
  }

  /**
   * Creates the operations column value.
   *
   * @param int $resourceID
   *   The resource id from fortnox.
   * @param string $param1
   *   Optional parameter.
   * @param string $param2
   *   Optional parameter.
   *
   * @return \Drupal\Component\Render\FormattableMarkup
   *   The column value.
   */
  public function createOperations($resourceID, $param1 = '', $param2 = '') {
    $renderer = \Drupal::service('renderer');
    $result = [
      '#type' => 'operations',
      '#links' => $this->getLinks($resourceID, $param1, $param2),
      '#theme' => 'links__dropbutton__operations',
    ];
    return new FormattableMarkup($renderer->render($result), []);
  }

}
