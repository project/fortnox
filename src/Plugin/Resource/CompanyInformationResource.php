<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\fortnox\Plugin\ResourceBase;

/**
 * Defines a plugin used to interact with fortnox company information resource.
 *
 * @Resource(
 *   id = "company-information",
 *   label = @Translation("Company Information Resource")
 * )
 */
class CompanyInformationResource extends ResourceBase {

  /**
   * {@inheritdoc}
   */
  protected $url = 'companyinformation';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'CompanyInformation';

  /**
   * {@inheritdoc}
   */
  public function addTableValues(array $values, array &$build) {
    if (!empty($values[$this->resourceIDPlural])) {
      $build['table'] = [
        '#type' => 'table',
        '#header' => array_keys($values[$this->resourceIDPlural]),
        '#rows' => [array_values($values[$this->resourceIDPlural])],
      ];
    }
  }

}
