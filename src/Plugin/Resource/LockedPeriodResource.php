<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\fortnox\Plugin\ResourceBase;

/**
 * Defines a plugin used to interact with fortnox locked period resources.
 *
 * @Resource(
 *   id = "locked-period",
 *   label = @Translation("Locked Period Resource")
 * )
 */
class LockedPeriodResource extends ResourceBase {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'LockedPeriod';

  /**
   * {@inheritdoc}
   */
  protected $url = 'settings/lockedperiod';

  /**
   * {@inheritdoc}
   */
  public function addTableValues(array $values, array &$build) {
    if (!empty($values[$this->resourceIDPlural])) {
      $build['table'] = ['#type' => 'table'];
      // Initialize the table.
      $build['table']['#header'] = array_keys($values[$this->resourceIDPlural]);
      $build['table']['#rows'][] = array_values($values[$this->resourceIDPlural]);
    }
  }

}
