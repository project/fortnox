<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox cost centers resources.
 *
 * @Resource(
 *   id = "cost-centers",
 *   label = @Translation("Cost Center Resource")
 * )
 */
class CostCentersResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Code';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'CostCenters';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'CostCenter';

  /**
   * {@inheritdoc}
   */
  protected $url = 'costcenters';

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      '@url',
      'Code',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    $links = parent::getLinks($resourceId, $param1, $param2);
    $links[] = [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];

    return $links;
  }

}
