<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox payment terms resources.
 *
 * @Resource(
 *   id = "terms-of-payment",
 *   label = @Translation("Terms Of Payment Resource Resource")
 * )
 */
class TermsOfPaymentResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'TermsOfPayments';

  /**
   * {@inheritdoc}
   */
  protected $url = 'termsofpayments';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'TermsOfPayment';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Code';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    $links = parent::getLinks($resourceId, $param1, $param2);
    $links[] = [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      '@url',
    ];
  }

}
