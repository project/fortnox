<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox assets resources.
 *
 * @Resource(
 *   id = "assets",
 *   label = @Translation("Assets")
 * )
 */
class AssetsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Assets';

  /**
   * {@inheritdoc}
   */
  protected $url = 'assets';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Assets';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Id';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    return [
      [
        'url' => Url::fromRoute('fortnox.edit_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
        'title' => $this->t('Edit'),
      ],
      [
        'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
        'title' => $this->t('Delete'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      '@url',
      'Status',
      'StatusId',
      'Type',
      'DepreciationFinal',
      'DepreciatedTo',
      'ManualOb',
      'Id',
      'Date',
      'EventId',
      'Amount',
      'UserId',
      'UserName',
      'Notes',
      'VoucherNumber',
      'VoucherSeries',
      'VoucherYear',
      'SupplierInvoice',
      'AccountedValue',
      'DepreciationAmount',
      'DepreciationPeriod',
    ];
  }

}
