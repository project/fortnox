<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox financial years resources.
 *
 * @Resource(
 *   id = "financial-years",
 *   label = @Translation("Financial Years")
 * )
 */
class FinancialYears extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'FinancialYears';

  /**
   * {@inheritdoc}
   */
  protected $url = 'financialyears';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'FinancialYear';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Id';

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      '@url',
      'Id',
    ];
  }

}
