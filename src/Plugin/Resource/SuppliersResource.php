<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\fortnox\Form\SuppliersForm;

/**
 * Defines a plugin used to interact with fortnox suppliers resources.
 *
 * @Resource(
 *   id = "suppliers",
 *   label = @Translation("Suppliers Resource")
 * )
 */
class SuppliersResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'SupplierNumber';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Suppliers';

  /**
   * {@inheritdoc}
   */
  protected $url = 'suppliers';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Supplier';

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      '@url',
      'Country',
      'SupplierNumber',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreateForm() {
    return $this->formBuilder->getForm(SuppliersForm::class);
  }

}
