<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox offers resources.
 *
 * @Resource(
 *   id = "offers",
 *   label = @Translation("Offers Resource")
 * )
 */
class OffersResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Offers';

  /**
   * {@inheritdoc}
   */
  protected $url = 'offers';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Offer';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'DocumentNumber';

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    $fields = parent::getDisabledFields();
    $resourceFields = [
      '@urlTaxReductionList',
      'AdministrationFeeVAT',
      'BasisTaxReduction',
      'Cancelled',
      'ContributionPercent',
      'ContributionValue',
      'FreightVAT',
      'Gross',
      'HouseWork',
      'InvoiceReference',
      'Net',
      'OrderReference',
      'RoundOff',
      'Sent',
      'TaxReduction',
      'Total',
      'TotalVat',
    ];
    $mergedFields = array_merge($fields, $resourceFields);

    return $mergedFields;
  }

}
