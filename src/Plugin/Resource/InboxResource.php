<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox inbox resources.
 *
 * @Resource(
 *   id = "inbox",
 *   label = @Translation("Inbox Resource")
 * )
 */
class InboxResource extends EmailSendersResource {

  /**
   * {@inheritdoc}
   */
  protected $url = 'inbox';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Folder';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Folder';

}
