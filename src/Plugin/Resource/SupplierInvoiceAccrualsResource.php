<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox invoice accruals resources.
 *
 * @Resource(
 *   id = "supplier-invoice-accruals",
 *   label = @Translation("Supplier Invoice Accruals Resource")
 * )
 */
class SupplierInvoiceAccrualsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'SupplierInvoiceAccruals';

  /**
   * {@inheritdoc}
   */
  protected $url = 'supplierinvoiceaccruals';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'SupplierInvoiceAccrual';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'SupplierInvoiceNumber';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    $links = parent::getLinks($resourceId, $param1, $param2);
    $links[] = [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      '@url',
      'Times',
    ];
  }

}
