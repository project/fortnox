<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox vouchers resources.
 *
 * @Resource(
 *   id = "voucher-file-connections",
 *   label = @Translation("Voucher File Connections Resource")
 * )
 */
class VoucherFileConnectionsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'VoucherFileConnections';

  /**
   * {@inheritdoc}
   */
  protected $url = 'voucherfileconnections';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'VoucerFileConnection';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'FileId';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    return [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];
  }

}
