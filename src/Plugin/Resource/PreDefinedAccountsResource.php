<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox predefined accounts resources.
 *
 * @Resource(
 *   id = "predefined-accounts",
 *   label = @Translation("Pre Defined Accounts Resource")
 * )
 */
class PreDefinedAccountsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'PreDefinedAccounts';

  /**
   * {@inheritdoc}
   */
  protected $url = 'predefinedaccounts';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'PreDefinedAccount';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Name';

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    $fields = parent::getDisabledFields();
    $resourceFields = [
      'Name',
    ];
    $mergedFields = array_merge($fields, $resourceFields);

    return $mergedFields;
  }

}
