<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Url;
use Drupal\fortnox\Plugin\ResourceBase;

/**
 * Defines a plugin used to interact with fortnox supplier invoice resources.
 *
 * @Resource(
 *   id = "supplier-invoices",
 *   label = @Translation("Supplier Invoices Resource")
 * )
 */
class SupplierInvoicesResource extends ResourceBase {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'GivenNumber';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'SupplierInvoices';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'SupplierInvoice';

  /**
   * {@inheritdoc}
   */
  protected $url = 'supplierinvoices';

  /**
   * {@inheritdoc}
   */
  protected $resourceFormID = 'supplier_invoice_form';

  /**
   * {@inheritdoc}
   */
  public function addTableValues(array $values, array &$build) {
    if (!empty($values[$this->resourceIDPlural])) {
      $build['table'] = ['#type' => 'table'];
      // Initialize the table.
      $build['table']['#header'] = array_keys(reset($values[$this->resourceIDPlural]));
      if (!empty($this->resourceIDPropertyName)) {
        $build['table']['#header'][] = 'Operations';
      }
      foreach ($values[$this->resourceIDPlural] as $value) {
        if (!empty($this->resourceIDPropertyName)) {
          if (!empty($value[$this->resourceIDPropertyName])) {
            $param1 = $this->getPluginId() == 'absence-transactions' ? $value['Date'] : '';
            $param2 = $this->getPluginId() == 'absence-transactions' ? $value['CauseCode'] : '';
            $value[] = $this->createOperations($value[$this->resourceIDPropertyName], $param1, $param2);
          }
        }
        $build['table']['#rows'][] = array_values($value);
      }
    }
  }

  /**
   * Creates the operations column value.
   *
   * @param int $resourceID
   *   The resource id from fortnox.
   * @param string $param1
   *   Optional parameter.
   * @param string $param2
   *   Optional parameter.
   *
   * @return \Drupal\Component\Render\FormattableMarkup
   *   The column value.
   */
  public function createOperations($resourceID, $param1 = '', $param2 = '') {
    $renderer = \Drupal::service('renderer');
    $result = [
      '#type' => 'operations',
      '#links' => $this->getLinks($resourceID, $param1, $param2),
      '#theme' => 'links__dropbutton__operations',
    ];
    return new FormattableMarkup($renderer->render($result), []);
  }

  /**
   * Creates the resource operation links.
   *
   * @param int $resourceId
   *   The resource id from fortnox.
   * @param string $param1
   *   Optional parameter.
   * @param string $param2
   *   Optional parameter.
   *
   * @return array
   *   The operation links.
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    return [
      [
        'url' => Url::fromRoute('fortnox.fortnox_edit_resource', [
          'resource' => $this->getPluginId(),
          'id' => $resourceId,
          'param1' => $param1,
          'param2' => $param2,
        ]),
        'title' => $this->t('Edit'),
      ],
    ];
  }

}
