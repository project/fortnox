<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox trusted domains resources.
 *
 * @Resource(
 *   id = "trusted-domains",
 *   label = @Translation("Trusted Domains")
 * )
 */
class TrustedDomains extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'TrustedDomains';

  /**
   * {@inheritdoc}
   */
  protected $url = 'emailtrusteddomains';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'TrustedDomain';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Id';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    return [
      [
        'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
        'title' => $this->t('Delete'),
      ],
    ];
  }

}
