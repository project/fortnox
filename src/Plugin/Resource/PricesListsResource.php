<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox prices lists resources.
 *
 * @Resource(
 *   id = "prices-lists",
 *   label = @Translation("Prices Lists Resource")
 * )
 */
class PricesListsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'PriceLists';

  /**
   * {@inheritdoc}
   */
  protected $url = 'pricelists';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'PriceList';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Code';

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    $fields = parent::getDisabledFields();
    $resourceFields = [
      'Code',
      'PreSelected',
    ];
    $mergedFields = array_merge($fields, $resourceFields);

    return $mergedFields;
  }

}
