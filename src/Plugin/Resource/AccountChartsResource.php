<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox account charts resources.
 *
 * @Resource(
 *   id = "account-charts",
 *   label = @Translation("Account Charts Resource")
 * )
 */
class AccountChartsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'AccountCharts';

  /**
   * {@inheritdoc}
   */
  protected $url = 'accountcharts';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = '';

}
