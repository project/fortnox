<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox labels resources.
 *
 * @Resource(
 *   id = "labels",
 *   label = @Translation("Labels Resource")
 * )
 */
class LabelsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Labels';

  /**
   * {@inheritdoc}
   */
  protected $url = 'labels';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Id';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    $links = parent::getLinks($resourceId, $param1, $param2);
    $links[] = [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      'Id',
    ];
  }

}
