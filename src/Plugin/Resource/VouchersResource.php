<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox vouchers resources.
 *
 * @Resource(
 *   id = "vouchers",
 *   label = @Translation("Vouchers Resource")
 * )
 */
class VouchersResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Vouchers';

  /**
   * {@inheritdoc}
   */
  protected $url = 'vouchers';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Voucher';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'VoucherNumber';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    return [
      [
        'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
        'title' => $this->t('Delete'),
      ],
    ];
  }

}
