<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox customers resources.
 *
 * @Resource(
 *   id = "customers",
 *   label = @Translation("Customers Resource")
 * )
 */
class CustomersResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'CustomerNumber';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Customer';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Customers';

  /**
   * {@inheritdoc}
   */
  protected $url = 'customers';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    $links = parent::getLinks($resourceId, $param1, $param2);
    $links[] = [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    $fields = parent::getDisabledFields();
    $resourceFields = [
      'DeliveryCountry',
    ];
    $mergedFields = array_merge($fields, $resourceFields);

    return $mergedFields;
  }

}
