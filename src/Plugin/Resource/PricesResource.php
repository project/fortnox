<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox prices resources.
 *
 * @Resource(
 *   id = "prices",
 *   label = @Translation("Prices Resource")
 * )
 */
class PricesResource extends ArchiveResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Prices';

  /**
   * {@inheritdoc}
   */
  protected $url = 'prices/sublist';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Price';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'ArticleNumber';

  /**
   * Returns an array containing the fields to be disabled in the edit form.
   *
   * @return array
   *   The fields array.
   */
  public static function getDisabledFields() {
    return [
      '@url',
      'Date',
    ];
  }

}
