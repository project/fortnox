<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox tax reductions resources.
 *
 * @Resource(
 *   id = "tax-reductions",
 *   label = @Translation("Tax Reductions Resource")
 * )
 */
class TaxReductionsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'TaxReductions';

  /**
   * {@inheritdoc}
   */
  protected $url = 'taxreductions';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'TaxReductions';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Id';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    $links = parent::getLinks($resourceId, $param1, $param2);
    $links[] = [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      '@url',
      'ApproveDAmount',
      'BilledAmount',
      'Id',
      'RequestSent',
      'VoucherNumber',
      'VoucherSeries',
      'VoucherYear',
    ];
  }

}
