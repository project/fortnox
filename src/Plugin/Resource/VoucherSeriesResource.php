<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox vouchers resources.
 *
 * @Resource(
 *   id = "voucher-series",
 *   label = @Translation("Voucher Series Resource")
 * )
 */
class VoucherSeriesResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'VoucherSeriesCollection';

  /**
   * {@inheritdoc}
   */
  protected $url = 'voucherseries';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'VoucherSeries';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Code';

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      '@url',
      'Code',
      'NextVoucherNumber',
      'Year',
    ];
  }

}
