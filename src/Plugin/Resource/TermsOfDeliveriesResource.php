<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox deliveries terms resources.
 *
 * @Resource(
 *   id = "terms-of-deliveries",
 *   label = @Translation("Terms Of Deliveries Resource")
 * )
 */
class TermsOfDeliveriesResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'TermsOfDeliveries';

  /**
   * {@inheritdoc}
   */
  protected $url = 'termsofdeliveries';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'TermsOfDelivery';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Code';

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      '@url',
    ];
  }

}
