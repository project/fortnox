<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox way of delivery resources.
 *
 * @Resource(
 *   id = "way-of-delivery",
 *   label = @Translation("Way of Delivery Resource")
 * )
 */
class WayOfDeliveryResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'WayOfDeliveries';

  /**
   * {@inheritdoc}
   */
  protected $url = 'wayofdeliveries';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'WayOfDelivery';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Code';

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      '@url',
      'Code',
    ];
  }

}
