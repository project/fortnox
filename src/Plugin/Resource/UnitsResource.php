<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox units resources.
 *
 * @Resource(
 *   id = "units",
 *   label = @Translation("Units Resource")
 * )
 */
class UnitsResource extends TermsOfPaymentResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Units';

  /**
   * {@inheritdoc}
   */
  protected $url = 'units';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Unit';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Code';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    $links = parent::getLinks($resourceId, $param1, $param2);
    $links[] = [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];

    return $links;
  }

}
