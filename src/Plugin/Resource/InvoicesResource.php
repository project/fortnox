<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox invoices resources.
 *
 * @Resource(
 *   id = "invoices",
 *   label = @Translation("Invoices Resource")
 * )
 */
class InvoicesResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Invoices';

  /**
   * {@inheritdoc}
   */
  protected $url = 'invoices';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Invoice';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'DocumentNumber';

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    $fields = parent::getDisabledFields();
    $resourceFields = [
      '@urlTaxReductionList',
      'AdministrationFeeVAT',
      'Balance',
      'BasisTaxReduction',
      'Booked',
      'Cancelled',
      'Credit',
      'CreditInvoiceReference',
      'ContractReference',
      'ContributionValue',
      'FinalPayDate',
      'FreightVAT',
      'Gross',
      'HouseWork',
      'InvoicePeriodStart',
      'InvoicePeriodEnd',
      'LastRemindDate',
      'Net',
      'NoxFinans',
      'OfferReference',
      'OrderReference',
      'OrganisationNumber',
      'Reminders',
      'RoundOff',
      'Sent',
      'TaxReduction',
      'Total',
      'TotalVAT',
      'VoucherNumber',
      'VoucherSeries',
      'VoucherYear',
    ];
    $mergedFields = array_merge($fields, $resourceFields);

    return $mergedFields;
  }

}
