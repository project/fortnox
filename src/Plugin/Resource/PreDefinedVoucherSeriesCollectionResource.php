<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox predefined voucher resources.
 *
 * @Resource(
 *   id = "predefined-voucher-series-collection",
 *   label = @Translation("Pre Defined Voucher Series Collection Resource")
 * )
 */
class PreDefinedVoucherSeriesCollectionResource extends PreDefinedAccountsResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'PreDefinedVoucherSeriesCollection';

  /**
   * {@inheritdoc}
   */
  protected $url = 'predefinedvoucherseries';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'PreDefinedVoucherSeries';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Name';

}
