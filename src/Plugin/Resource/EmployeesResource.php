<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\fortnox\Form\EmployeesForm;

/**
 * Defines a plugin used to interact with fortnox employees resources.
 *
 * @Resource(
 *   id = "employees",
 *   label = @Translation("Employees")
 * )
 */
class EmployeesResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Employees';

  /**
   * {@inheritdoc}
   */
  protected $url = 'employees';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Employee';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'EmployeeId';

  /**
   * {@inheritdoc}
   */
  public function getCreateForm() {
    return $this->formBuilder->getForm(EmployeesForm::class);
  }

}
