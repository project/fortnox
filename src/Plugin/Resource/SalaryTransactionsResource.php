<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox salary transactions resources.
 *
 * @Resource(
 *   id = "salary-transactions",
 *   label = @Translation("Salary Transactions Resource")
 * )
 */
class SalaryTransactionsResource extends CompanyInformationResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'SalaryTransactions';

  /**
   * {@inheritdoc}
   */
  protected $url = 'salarytransactions';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'SalaryTransaction';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'SalaryRow';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    $links = parent::getLinks($resourceId, $param1, $param2);
    $links[] = [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];

    return $links;
  }

  /**
   * Returns an array that contains the field to be disabled in the edit form.
   *
   * @return array
   *   The fields array.
   */
  public static function getDisabledFields() {
    return [
      'SalaryRow',
    ];
  }

}
