<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox schedules resources.
 *
 * @Resource(
 *   id = "schedule-times",
 *   label = @Translation("Schedule Times Resource")
 * )
 */
class ScheduleTimesResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'ScheduleId';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'ScheduleTimes';

  /**
   * {@inheritdoc}
   */
  protected $url = 'scheduletimes';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'ScheduleTime';

}
