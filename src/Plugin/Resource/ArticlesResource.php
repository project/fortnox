<?php

namespace Drupal\fortnox\Plugin\Resource;

use Drupal\Core\Url;

/**
 * Defines a plugin used to interact with fortnox article resources.
 *
 * @Resource(
 *   id = "articles",
 *   label = @Translation("Articles Resource")
 * )
 */
class ArticlesResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'ArticleNumber';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'Article';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'Articles';

  /**
   * {@inheritdoc}
   */
  protected $url = 'articles';

  /**
   * {@inheritdoc}
   */
  protected function getLinks($resourceId, $param1 = '', $param2 = '') {
    $links = parent::getLinks($resourceId, $param1, $param2);
    $links[] = [
      'url' => Url::fromRoute('fortnox.delete_resource', ['resource' => $this->getPluginId(), 'id' => $resourceId]),
      'title' => $this->t('Delete'),
    ];

    return $links;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    return [
      '@url',
      'DisposableQuantity',
      'ReservedQuantity',
      'SalesPrice',
      'StockValue',
      'SupplierName',
    ];
  }

}
