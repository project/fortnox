<?php

namespace Drupal\fortnox\Plugin\Resource;

/**
 * Defines a plugin used to interact with fortnox payment modes resources.
 *
 * @Resource(
 *   id = "modes-of-payments",
 *   label = @Translation("Modes Of Payments Resource")
 * )
 */
class ModesOfPaymentsResource extends SupplierInvoicesResource {

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPlural = 'ModesOfPayments';

  /**
   * {@inheritdoc}
   */
  protected $url = 'modesofpayments';

  /**
   * {@inheritdoc}
   */
  protected $resourceIDPropertyName = 'Code';

  /**
   * {@inheritdoc}
   */
  public $resourceIDSingular = 'ModeOfPayment';

  /**
   * {@inheritdoc}
   */
  public static function getDisabledFields() {
    $fields = parent::getDisabledFields();
    $resourceFields = [
      'Code',
    ];
    $mergedFields = array_merge($fields, $resourceFields);

    return $mergedFields;
  }

}
