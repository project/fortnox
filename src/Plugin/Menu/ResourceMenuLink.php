<?php

namespace Drupal\fortnox\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;

/**
 * Class ResourceMenuLink.
 */
class ResourceMenuLink extends MenuLinkDefault {}
