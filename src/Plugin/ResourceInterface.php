<?php

namespace Drupal\fortnox\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Resource plugins.
 */
interface ResourceInterface extends PluginInspectionInterface {

  /**
   * Generates a table renderable array containing fortnox resources.
   *
   * @return array
   *   A table renderable array of fortnox resources.
   */
  public function getTableListing();

  /**
   * Gets the response from the request to the Fortnox API.
   *
   * @param array $build
   *   Contains the renderable table array.
   */
  public function getResponse(array &$build);

  /**
   * Adds response values in the table renderable array.
   *
   * @param array $values
   *   The values to be added the table renderable array.
   * @param array $build
   *   Contains the renderable table array.
   *
   * @return mixed
   *   Returns the table containing the values.
   */
  public function addTableValues(array $values, array &$build);

}
