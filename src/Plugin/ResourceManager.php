<?php

namespace Drupal\fortnox\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Resource plugin manager.
 */
class ResourceManager extends DefaultPluginManager {

  /**
   * Constructs a new ResourceManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Resource', $namespaces, $module_handler, 'Drupal\fortnox\Plugin\ResourceInterface', 'Drupal\fortnox\Annotation\Resource');

    $this->alterInfo('fortnox_resource_info');
    $this->setCacheBackend($cache_backend, 'fortnox_resource_plugins');
  }

}
