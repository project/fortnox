<?php

namespace Drupal\fortnox\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\fortnox\Plugin\ResourceManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derivative class that provides the menu links for the Resources plugins.
 */
class ResourceMenuLink extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The resource plugin manager.
   *
   * @var \Drupal\fortnox\Plugin\ResourceManager
   */
  protected $resourceManager;

  /**
   * Creates a ResourceMenuLink instance.
   *
   * @param string $base_plugin_id
   *   The plugin id.
   * @param \Drupal\fortnox\Plugin\ResourceManager $resource_manager
   *   The resource manager service.
   */
  public function __construct($base_plugin_id, ResourceManager $resource_manager) {
    $this->resourceManager = $resource_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('plugin.manager.resource')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    $products = $this->resourceManager->getDefinitions();

    foreach ($products as $id => $product) {
      $links[$id] = [
        'title' => isset($product['label']) ? $product['label'] : 'Resource',
        'route_name' => 'fortnox.fortnox_supplier_invoices',
        'route_parameters' => ['resource' => $id],
        'parent' => 'fortnox.admin_resources',
        'expanded' => FALSE,
      ] + $base_plugin_definition;
    }

    return $links;
  }

}
