<?php

namespace Drupal\fortnox\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fortnox\Plugin\ResourceTrait;

/**
 * Provides resource creation for fortnox accounts.
 */
class ContractsForm extends ResourceFormBase {

  use ResourceTrait;

  /**
   * {@inheritdoc}
   */
  protected $fields = [
    'radios' => [
      'Active' => FALSE,
      'Continuous' => FALSE,
      'VATIncluded' => FALSE,
    ],
    'number' => [
      'AdministrationFee' => FALSE,
      'ContractLength' => TRUE,
      'InvoiceDiscount' => FALSE,
      'InvoiceInterval' => TRUE,
    ],
    'textfield' => [
      'Comments' => FALSE,
      'Freight' => TRUE,
      'ExternalInvoiceReference1' => FALSE,
      'ExternalInvoiceReference2' => FALSE,
      'OurReference' => FALSE,
      'Remarks' => FALSE,
    ],
    'datelist' => [
      'ContractDate' => FALSE,
      'PeriodEnd' => TRUE,
      'PeriodStart' => TRUE,
    ],
    'select' => [
      'CostCenter' => FALSE,
      'Currency' => FALSE,
      'Customer' => TRUE,
      'Language' => FALSE,
      'PriceList' => FALSE,
      'Project' => FALSE,
      'Template' => FALSE,
      'TermsOfDelivery' => FALSE,
      'TermsOfPayment' => FALSE,
      'WayOfDelivery' => FALSE,
      'YourOrderNumber' => FALSE,
    ],
    'fieldset' => [
      'EmailInformation' => FALSE,
      'InvoiceRows' => TRUE,
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contracts_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $parameters = $this->getRouteMatch()->getParameters()->all();
    $response = [];
    if (!empty($parameters['id'])) {
      $build = [];
      $id = $parameters['id'];
      $submitButtonValue = $this->t('Edit Contract');
      if (!empty($parameters['param1']) && !empty($parameters['param2'])) {
        $id .= '/' . $parameters['param1'] . '/' . $parameters['param2'];
      }
      if (empty($form_state->get('response'))) {
        $response = $parameters['resource']->getResponse($build, $id);
        $form_state->set('response', $response);
      }
      else {
        $response = $form_state->get('response');
      }
    }
    else {
      $submitButtonValue = $this->t('Create Contract');
    }
    $values = isset($response['Contract']) ? $response['Contract'] : [];
    $this->createFormFields($form, $values);
    $form['CostCenter']['#options'] = !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('cost-centers', 'Description') : $form_state->getCompleteForm()['CostCenter']['#options'];
    $form['Currency']['#options'] = !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('currencies', 'Description') : $form_state->getCompleteForm()['Currency']['#options'];
    $form['Customer']['#options'] = !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('customers', 'Name') : $form_state->getCompleteForm()['Customer']['#options'];
    $form['Language']['#options'] = [
      'SV' => $this->t('Swedish'),
      'EN' => $this->t('English'),
    ];
    $form['PriceList']['#options'] = !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('prices-lists', 'Description') : $form_state->getCompleteForm()['PriceList']['#options'];
    $form['Project']['#options'] = !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('projects', 'Description') : $form_state->getCompleteForm()['Project']['#options'];
    $form['Template']['#options'] = !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('contract-templates', 'ContractTemplateName') : $form_state->getCompleteForm()['Template']['#options'];
    $form['TermsOfDelivery']['#options'] = !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('terms-of-deliveries', 'Description') : $form_state->getCompleteForm()['TermsOfDelivery']['#options'];
    $form['TermsOfPayment']['#options'] = !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('terms-of-payment', 'Description') : $form_state->getCompleteForm()['TermsOfPayment']['#options'];
    $form['WayOfDelivery']['#options'] = !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('way-of-delivery', 'Description') : $form_state->getCompleteForm()['WayOfDelivery']['#options'];
    $form['YourOrderNumber']['#options'] = !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('orders', 'DocumentNumber') : $form_state->getCompleteForm()['YourOrderNumber']['#options'];
    $form['Customer']['#default_value'] = isset($values['CustomerNumber']) ? $values['CustomerNumber'] : '';
    // Add the fields for email information fieldset.
    $form['EmailInformation']['#tree'] = TRUE;
    $emailInformationFields = [
      'textfield' => [
        'EmailAddressTo' => FALSE,
        'EmailAddressFrom' => FALSE,
        'EmailAddressCC' => FALSE,
        'EmailAddressBCC' => FALSE,
        'EmailSubject' => FALSE,
        'EmailBody' => FALSE,
      ],
    ];
    $emailInformationValues = !empty($response['Contract']) ? $response['Contract']['EmailInformation'] : [];
    $this->createFormFields($form['EmailInformation'], $emailInformationValues, $emailInformationFields);

    // Add the fields for invoice rows fieldset.
    $invoiceRowsValues = !empty($response['Contract']) ? $response['Contract']['InvoiceRows'] : [];
    $form['#tree'] = TRUE;
    $form['InvoiceRows'] = [
      '#type' => 'dynamic_add_more',
      '#title' => $this->t('Invoice Rows'),
      '#fields' => [
        [
          'field_name' => 'AccountNumber',
          'field_label' => $this->t('AccountNumber'),
          'field_type' => 'select',
          'options' => !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('accounts', 'Description', ['Active' => TRUE]) : $form_state->getCompleteForm()['InvoiceRows'][0]['AccountNumber']['#options'],
          'required' => TRUE,
        ],
        [
          'field_name' => 'ArticleNumber',
          'field_label' => $this->t('ArticleNumber'),
          'field_type' => 'select',
          'options' => !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('articles', 'Description') : $form_state->getCompleteForm()['InvoiceRows'][0]['ArticleNumber']['#options'],
          'required' => TRUE,
        ],
        [
          'field_name' => 'CostCenter',
          'field_label' => $this->t('CostCenter'),
          'field_type' => 'select',
          'options' => !$form_state->isRebuilding() ? $form['CostCenter']['#options'] : $form_state->getCompleteForm()['CostCenter']['#options'],
        ],
        [
          'field_name' => 'Project',
          'field_label' => $this->t('Project'),
          'field_type' => 'select',
          'options' => !$form_state->isRebuilding() ? $form['Project']['#options'] : $form_state->getCompleteForm()['Project']['#options'],
        ],
        [
          'field_name' => 'DiscountType',
          'field_label' => $this->t('DiscountType'),
          'field_type' => 'select',
          'options' => self::getDiscountTypesList(),
        ],
        [
          'field_name' => 'Unit',
          'field_label' => $this->t('Unit'),
          'field_type' => 'select',
          'options' => !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('units', 'Description') : $form_state->getCompleteForm()['InvoiceRows'][0]['Unit']['#options'],
        ],
        [
          'field_name' => 'Description',
          'field_label' => $this->t('Description'),
          'field_type' => 'textfield',
        ],
        [
          'field_name' => 'DeliveredQuantity',
          'field_label' => $this->t('DeliveredQuantity'),
          'field_type' => 'number',
        ],
        [
          'field_name' => 'Discount',
          'field_label' => $this->t('Discount'),
          'field_type' => 'number',
        ],
        [
          'field_name' => 'Price',
          'field_label' => $this->t('Price'),
          'field_type' => 'number',
        ],
        [
          'field_name' => 'VAT',
          'field_label' => $this->t('VAT'),
          'field_type' => 'number',
        ],
      ],
      '#fieldset_machine_name' => 'InvoiceRows',
      '#default_value' => $invoiceRowsValues,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $submitButtonValue,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $date = $form_state->getValue('ContractDate');
    if ($date instanceof DrupalDateTime) {
      $form_state->setValue('ContractDate', $date->format('Y-m-d'));
    }
    $startPeriod = $form_state->getValue('PeriodStart');
    if ($startPeriod instanceof DrupalDateTime) {
      $form_state->setValue('PeriodStart', $startPeriod->format('Y-m-d'));
    }
    $endPeriod = $form_state->getValue('PeriodEnd');
    if ($endPeriod instanceof DrupalDateTime) {
      $form_state->setValue('PeriodEnd', $endPeriod->format('Y-m-d'));
    }
    if ($startPeriod instanceof DrupalDateTime && $endPeriod instanceof DrupalDateTime) {
      $startPeriod = $startPeriod->format('Y-m-d');
      $endPeriod = $endPeriod->format('Y-m-d');
      $startPeriod = strtotime($startPeriod);
      $endPeriod = strtotime($endPeriod);
      if ($startPeriod - $endPeriod > 0) {
        $form_state->setError($form['PeriodStart'], $this->t('PeriodStart should be lower than PeriodEnd.'));
      }
    }
    $contractLength = $form_state->getValue('ContractLength');
    $invoiceLength = $form_state->getValue('InvoiceInterval');
    if ($invoiceLength > $contractLength) {
      $form_state->setErrorByName('InvoiceInterval', $this->t('InvoiceInterval should be lower than ContractLength.'));
    }
    $customer = $form_state->getValue('Customer');
    if (!empty($customer)) {
      $form_state->setValue('CustomerNumber', $customer);
      $form_state->unsetValue('Customer');
    }
    else {
      $form_state->setErrorByName('Customer', $this->t('Please select a customer.'));
    }
    $template = $form_state->getValue('Template');
    if (!empty($template)) {
      $form_state->setValue('TemplateNumber', $template);
    }
    $form_state->unsetValue('Template');
  }

}
