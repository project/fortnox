<?php

namespace Drupal\fortnox\Form;

use CommerceGuys\Addressing\AddressFormat\AddressField;
use CommerceGuys\Addressing\AddressFormat\FieldOverride;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fortnox\Plugin\ResourceTrait;

/**
 * Class SuppliersForm.
 */
class SuppliersForm extends ResourceFormBase {

  use ResourceTrait;

  /**
   * {@inheritdoc}
   */
  protected $fields = [
    'checkbox' => [
      'Active' => FALSE,
      'DisablePaymentFile' => FALSE,
    ],
    'textfield' => [
      'Bank' => FALSE,
      'BG' => FALSE,
      'BIC' => FALSE,
      'BranchCode' => FALSE,
      'CostCenter' => FALSE,
      'Fax' => FALSE,
      'IBAN' => FALSE,
      'Name' => TRUE,
      'OrganisationNumber' => FALSE,
      'OurReference' => FALSE,
      'OurCustomerNumber' => FALSE,
      'PG' => FALSE,
      'VATNumber' => FALSE,
      'VATType' => FALSE,
      'WorkPlace' => FALSE,
      'WWW' => FALSE,
      'YourReference' => FALSE,
      'BankAccountNumber' => FALSE,
      'ClearingNumber' => FALSE,
    ],
    'email' => [
      'Email' => FALSE,
    ],
    'fieldset' => [
      'InvoiceAddress' => FALSE,
      'VisitingAddresses' => FALSE,
    ],
    'tel' => [
      'Phone1' => FALSE,
      'Phone2' => FALSE,
    ],
    'textarea' => [
      'Comments' => FALSE,
    ],
    'select' => [
      'CostCenter' => FALSE,
      'PreDefinedAccount' => FALSE,
      'Project' => FALSE,
      'TermsOfPayment' => FALSE,
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'suppliers_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Check if we are on edit form and add default values if so.
    $parameters = $this->getRouteMatch()->getParameters()->all();
    $response = [];
    // Get resource ID and use it to get resource values.
    if (!empty($parameters['id'])) {
      $build = [];
      $id = $parameters['id'];
      if (!empty($parameters['param1']) && !empty($parameters['param2'])) {
        $id .= '/' . $parameters['param1'] . '/' . $parameters['param2'];
      }
      $response = $parameters['resource']->getResponse($build, $id);
      $submitButtonValue = $this->t('Edit Supplier');
    }
    else {
      $submitButtonValue = $this->t('Create Supplier');
    }
    // If values are found for the resource, create the fields with default
    // values.
    $values = isset($response['Supplier']) ? $response['Supplier'] : [];
    $currencyDefaultValue = isset($values['Currency']) ? $values['Currency'] : '';
    $this->createCurrencyFields($form, $currencyDefaultValue);
    $this->createFormFields($form, $values);
    $form['InvoiceAddress']['#tree'] = FALSE;
    $form['InvoiceAddress']['InvoiceAddress'] = [
      '#type' => 'address',
      '#field_overrides' => [
        AddressField::GIVEN_NAME => FieldOverride::HIDDEN,
        AddressField::FAMILY_NAME => FieldOverride::HIDDEN,
        AddressField::ORGANIZATION => FieldOverride::HIDDEN,
      ],
    ];
    $form['VisitingAddresses']['VisitingAddresses'] = [
      '#type' => 'address',
      '#field_overrides' => [
        AddressField::GIVEN_NAME => FieldOverride::HIDDEN,
        AddressField::FAMILY_NAME => FieldOverride::HIDDEN,
        AddressField::ORGANIZATION => FieldOverride::HIDDEN,
        AddressField::ADDRESS_LINE2 => FieldOverride::HIDDEN,
      ],
    ];
    if (!empty($values)) {
      $form['InvoiceAddress']['InvoiceAddress']['#default_value'] = [
        'country_code' => $values['CountryCode'],
        'locality' => $values['City'],
        'postal_code' => $values['ZipCode'],
        'address_line1' => $values['Address1'],
        'address_line2' => $values['Address2'],
      ];
      $form['VisitingAddresses']['VisitingAddresses']['#default_value'] = [
        'country_code' => $values['VisitingCountryCode'],
        'locality' => $values['VisitingCity'],
        'postal_code' => $values['VisitingZipCode'],
        'address_line1' => $values['VisitingAddress'],
      ];
    }

    // Get the options for the select list fields.
    $form['CostCenter']['#options'] = !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('cost-centers', 'Description', ['Active' => TRUE]) : $form_state->getCompleteForm()['CostCenter']['#options'];
    $form['PreDefinedAccount']['#options'] = !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('accounts', 'Description') : $form_state->getCompleteForm()['PreDefinedAccount']['Options'];
    $form['Project']['#options'] = !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('projects', 'Description') : $form_state->getCompleteForm()['Project']['Options'];
    $form['TermsOfPayment']['#options'] = !$form_state->isRebuilding() ? $this->getDynamicResourceOptions('terms-of-payment', 'Description') : $form_state->getCompleteForm()['TermsOfPayment']['Options'];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $submitButtonValue,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Remove form token and id from form state.
    $form_state->cleanValues();
    $resource = $this->requestStack->getCurrentRequest()->get('resource');
    $resourceIDSingular = $resource->resourceIDSingular;
    $values[$resourceIDSingular] = $form_state->getValues();
    if (!empty($values[$resourceIDSingular]['InvoiceAddress'])) {
      $values[$resourceIDSingular]['Address1'] = $values[$resourceIDSingular]['InvoiceAddress']['address_line1'];
      $values[$resourceIDSingular]['Address2'] = $values[$resourceIDSingular]['InvoiceAddress']['address_line2'];
      $values[$resourceIDSingular]['ZipCode'] = $values[$resourceIDSingular]['InvoiceAddress']['postal_code'];
      $values[$resourceIDSingular]['City'] = $values[$resourceIDSingular]['InvoiceAddress']['locality'];
      $values[$resourceIDSingular]['CountryCode'] = $values[$resourceIDSingular]['InvoiceAddress']['country_code'];
      unset($values[$resourceIDSingular]['InvoiceAddress']);
    }
    if (!empty($values[$resourceIDSingular]['VisitingAddresses'])) {
      $values[$resourceIDSingular]['VisitingAddress'] = $values[$resourceIDSingular]['VisitingAddresses']['address_line1'];
      $values[$resourceIDSingular]['VisitingCity'] = $values[$resourceIDSingular]['VisitingAddresses']['locality'];
      $values[$resourceIDSingular]['VisitingCountryCode'] = $values[$resourceIDSingular]['VisitingAddresses']['country_code'];
      $values[$resourceIDSingular]['VisitingZipCode'] = $values[$resourceIDSingular]['VisitingAddresses']['postal_code'];
      unset($values[$resourceIDSingular]['VisitingAddresses']);
    }
    $parameters = $this->getRouteMatch()->getRawParameters()->all();
    if (!empty($parameters['id'])) {
      $id = $parameters['id'];
      if (!empty($parameters['param1']) && !empty($parameters['param2'])) {
        $id .= '/' . $parameters['param1'] . '/' . $parameters['param2'];
      }
      $resource->updateResource($id, $values);
    }
    else {
      $resource->createFortnoxResource($values);
    }
  }

}
