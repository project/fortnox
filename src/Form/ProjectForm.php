<?php

namespace Drupal\fortnox\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fortnox\Plugin\ResourceTrait;

/**
 * Provides a form for Fortnox project resource edit or create.
 */
class ProjectForm extends ResourceFormBase {

  use ResourceTrait;

  /**
   * {@inheritdoc}
   */
  protected $fields = [
    'textfield' => [
      'Description' => TRUE,
      'ProjectLeader' => FALSE,
      'ProjectNumber' => FALSE,
    ],
    'datelist' => [
      'EndDate' => FALSE,
      'StartDate' => FALSE,
    ],
    'textarea' => [
      'Comments' => FALSE,
    ],
    'select' => [
      'Status' => FALSE,
      'ContactPerson' => FALSE,
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'project_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Check if we are on edit form and add default values if so.
    $parameters = $this->getRouteMatch()->getParameters()->all();
    $response = [];
    // Get resource ID and use it to get resource values.
    if (!empty($parameters['id'])) {
      $build = [];
      $id = $parameters['id'];
      $submitButtonValue = $this->t('Edit Project');
      if (!empty($parameters['param1']) && !empty($parameters['param2'])) {
        $id .= '/' . $parameters['param1'] . '/' . $parameters['param2'];
      }
      $response = $parameters['resource']->getResponse($build, $id);
    }
    else {
      $submitButtonValue = $this->t('Create Project');
    }
    // If values are found for the resource, create the fields with default
    // values.
    $values = isset($response['Project']) ? $response['Project'] : [];
    $this->createFormFields($form, $values);

    $form['Status']['#options'] = $this->getSelectListOptions('Status');
    $form['ContactPerson']['#options'] = $this->getSelectListOptions('ContactPerson');
    $form['ProjectNumber']['#access'] = FALSE;

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $submitButtonValue,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $startDate = $form_state->getValue('StartDate');
    $endDate = $form_state->getValue('EndDate');
    if ($startDate instanceof DrupalDateTime) {
      $form_state->setValue('StartDate', $startDate->format('Y-m-d'));
    }
    if ($endDate instanceof DrupalDateTime) {
      $form_state->setValue('EndDate', $endDate->format('Y-m-d'));
    }
    if ($startDate instanceof DrupalDateTime && $endDate instanceof DrupalDateTime) {
      $startDate = $startDate->format('Y-m-d');
      $endDate = $endDate->format('Y-m-d');
      $startDate = strtotime($startDate);
      $endDate = strtotime($endDate);
      if ($startDate - $endDate > 0) {
        $form_state->setError($form['StartDate'], $this->t('StartDate should be lower than EndDate.'));
      }
    }
  }

}
