<?php

namespace Drupal\fortnox\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\fortnox\Plugin\ResourceManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the delete form for Fortnox resources.
 */
class DeleteResource extends ConfirmFormBase {

  /**
   * The resource plugin manager.
   *
   * @var \Drupal\fortnox\Plugin\ResourceManager
   */
  protected $resourceManager;

  /**
   * ResourcesListing constructor.
   *
   * @param \Drupal\fortnox\Plugin\ResourceManager $resource_manager
   *   The resource plugin manager.
   */
  public function __construct(ResourceManager $resource_manager) {
    $this->resourceManager = $resource_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.resource')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'delete_resource_form';
  }

  /**
   * Returns the question to ask the user.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form question. The page title will be set to this value.
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this resource?');
  }

  /**
   * Returns the route to go to if the user cancels the action.
   *
   * @return \Drupal\Core\Url
   *   A URL object.
   */
  public function getCancelUrl() {
    $resource = $this->getRouteMatch()->getRawParameter('resource');

    return Url::fromUserInput('/fortnox/' . $resource);
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $parameters = $this->getRouteMatch()->getRawParameters();
    $queryParameters = $this->requestStack->getCurrentRequest()->query->all();
    if ($form_state->getValue('confirm') == 1) {
      try {
        $plugin = $this->resourceManager->createInstance($parameters->get('resource'));
        if (is_object($plugin)) {
          $param1 = !empty($queryParameters['param1']) ? $queryParameters['param1'] : '';
          $param2 = !empty($queryParameters['param2']) ? $queryParameters['param2'] : '';
          $response = $plugin->deleteResource($parameters->get('id'), $param1, $param2);
          if (!is_bool($response)) {
            $this->messenger()->addStatus($this->t('The resource was deleted.'));
          }
          else {
            $this->messenger()->addError($this->t('There was an error when trying to delete this resource. Please try again.'));
          }
        }
        $form_state->setRedirect('fortnox.fortnox_supplier_invoices', ['resource' => $parameters->get('resource')]);
      }
      catch (PluginException $e) {
        $this->logger('fortnox')->error($e);
      }
    }
  }

}
