<?php

namespace Drupal\fortnox\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\fortnox\Plugin\ResourceTrait;

/**
 * Provides resource creation for fortnox accounts.
 */
class AccountsForm extends ResourceFormBase {

  use ResourceTrait;

  /**
   * {@inheritdoc}
   */
  protected $fields = [
    'checkbox' => [
      'Active' => FALSE,
    ],
    'number' => [
      'BalanceBroughtForward' => FALSE,
      'Number' => TRUE,
      'SRU' => FALSE,
    ],
    'select' => [
      'Project' => FALSE,
      'CostCenter' => FALSE,
      'CostCenterSettings' => FALSE,
      'ProjectSettings' => FALSE,
      'TransactionInformationSettings' => FALSE,
    ],
    'textfield' => [
      'Description' => FALSE,
      'TransactionInformation' => FALSE,
      'VATCode' => FALSE,
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'accounts_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $parameters = $this->getRouteMatch()->getParameters()->all();
    $response = [];
    if (!empty($parameters['id'])) {
      $build = [];
      $id = $parameters['id'];
      $submitButtonValue = $this->t('Edit Account');
      if (!empty($parameters['param1']) && !empty($parameters['param2'])) {
        $id .= '/' . $parameters['param1'] . '/' . $parameters['param2'];
      }
      $response = $parameters['resource']->getResponse($build, $id);
    }
    else {
      $submitButtonValue = $this->t('Create Account');
    }
    $values = isset($response['Account']) ? $response['Account'] : [];
    $this->createFormFields($form, $values);
    $form['Project']['#options'] = $this->getDynamicResourceOptions('projects', 'Description');
    $form['CostCenter']['#options'] = $this->getDynamicResourceOptions('cost-centers', 'Code');
    $form['CostCenterSettings']['#options'] = self::getSettingsFieldOptions();
    $form['ProjectSettings']['#options'] = self::getSettingsFieldOptions();
    $form['TransactionInformationSettings']['#options'] = self::getSettingsFieldOptions();
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $submitButtonValue,
    ];

    return $form;
  }

}
