<?php

namespace Drupal\fortnox\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fortnox\Plugin\ResourceManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ResourceFormBase.
 */
abstract class ResourceFormBase extends FormBase {

  /**
   * The resource plugin manager.
   *
   * @var \Drupal\fortnox\Plugin\ResourceManager
   */
  protected $resourceManager;

  /**
   * The current request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * An array of resource fields keyed by drupal field type.
   *
   * @var array
   */
  protected $fields;

  /**
   * ResourcesListing constructor.
   *
   * @param \Drupal\fortnox\Plugin\ResourceManager $resourceManager
   *   The resource manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(ResourceManager $resourceManager, RequestStack $request_stack) {
    $this->resourceManager = $resourceManager;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.resource'),
      $container->get('request_stack')
    );
  }

  /**
   * Create form textfields.
   *
   * @param array $form
   *   The form array.
   * @param array $resourceValues
   *   Current resource value, if we are on edit page.
   * @param array $fieldNames
   *   An array of fields to be created.
   *
   * @return array
   *   The form array.
   */
  protected function createFormFields(array &$form, array $resourceValues = [], array $fieldNames = []) {
    $fieldNames = !empty($fieldNames) ? $fieldNames : $this->fields;
    foreach ($fieldNames as $fieldType => $fields) {
      if (is_array($fields)) {
        foreach ($fields as $key => $required) {
          $form[$key] = [
            '#type' => $fieldType,
            '#title' => $key,
            '#required' => $required,
            '#default_value' => isset($resourceValues[$key]) ? $resourceValues[$key] : '',
          ];
          if ($fieldType == 'datelist') {
            $form[$key]['#date_part_order'] = ['day', 'month', 'year'];
            $form[$key]['#default_value'] = isset($resourceValues[$key]) ? new DrupalDateTime($resourceValues[$key]) : new DrupalDateTime();
          }
          elseif ($fieldType == 'radios') {
            $form[$key]['#options'] = [
              FALSE => $this->t('No'),
              TRUE => $this->t('Yes'),
            ];
          }
          elseif ($fieldType == 'tel') {
            $form[$key]['#pattern'] = '^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$';
          }
        }
      }
    }
    // Make sure that the fields are sorted alphabetically.
    ksort($form);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Remove form token and id from form state.
    $form_state->cleanValues();
    $resource = $this->requestStack->getCurrentRequest()->get('resource');
    $resourceIDSingular = $resource->resourceIDSingular;
    $values[$resourceIDSingular] = $form_state->getValues();
    if (isset($values[$resourceIDSingular]['SupplierInvoiceRows']['actions'])) {
      unset($values[$resourceIDSingular]['SupplierInvoiceRows']['actions']);
    }
    if (isset($values[$resourceIDSingular]['InvoiceRows']['actions'])) {
      unset($values[$resourceIDSingular]['InvoiceRows']['actions']);
    }
    if (isset($values[$resourceIDSingular]['actions'])) {
      unset($values[$resourceIDSingular]['actions']);
    }
    $parameters = $this->getRouteMatch()->getRawParameters()->all();
    if (!empty($parameters['id'])) {
      $id = $parameters['id'];
      if (!empty($parameters['param1']) && !empty($parameters['param2'])) {
        $id .= '/' . $parameters['param1'] . '/' . $parameters['param2'];
      }
      $resource->updateResource($id, $values);
    }
    else {
      $resource->createFortnoxResource($values);
    }
  }

}
