<?php

namespace Drupal\fortnox\Routing;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\fortnox\Plugin\ResourceManager;
use Symfony\Component\Routing\Route;

/**
 * Converts parameters for upcasting resource IDs to full objects.
 */
class ParamConverter implements ParamConverterInterface {

  /**
   * The resource plugin manager.
   *
   * @var \Drupal\fortnox\Plugin\ResourceManager
   */
  protected $resourceManager;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * ResourcesListing constructor.
   *
   * @param \Drupal\fortnox\Plugin\ResourceManager $resource_manager
   *   The resource plugin manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Drupal messages logger.
   */
  public function __construct(ResourceManager $resource_manager, LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->resourceManager = $resource_manager;
    $this->logger = $loggerChannelFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    if (!empty($value)) {
      if (is_object($value)) {
        return $value;
      }
      try {
        $plugin = $this->resourceManager->createInstance($value);

        return $plugin;
      }
      catch (PluginException $e) {
        $this->logger->get('fortnox')->error($e);
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return $definition['type'] == 'fortnox-resource' && $name == 'resource';
  }

}
