<?php

namespace Drupal\fortnox\Services;

/**
 * Defines an interface for the fortnox client.
 */
interface FortnoxClientInterface {

  /**
   * Sets the client secret on the client.
   *
   * @param string $clientSecret
   *   The client secret string.
   */
  public function setClientSecret($clientSecret);

  /**
   * Sets the access token on the client.
   *
   * @param string $accessToken
   *   The access token string.
   */
  public function setAccessToken($accessToken);

  /**
   * Sets the integration api code on the client.
   *
   * @param string $apiCode
   *   The integration api code string.
   */
  public function setIntegrationApiCode($apiCode);

  /**
   * Makes requests to fortnox api.
   *
   * @param string $method
   *   The request method.
   * @param string $endpointURL
   *   The request endpoint URL.
   * @param array $requestParameters
   *   The request parameters to be sent (i.e. headers, body).
   *
   * @return mixed
   *   Returns the json decoded request response body.
   */
  public function makeRequest($method, $endpointURL, array $requestParameters);

  /**
   * Makes a request to fortnox api to enable the current integration.
   *
   * @return string|bool
   *   Returns the access token if the integration was successfully enabled,
   *   FALSE otherwise.
   */
  public function enableIntegration();

  /**
   * Checks if integration is enabled.
   *
   * @return bool
   *   If the integration is enabled returns TRUE, FALSE otherwise
   */
  public function checkIfIntegrationIsEnabled();

}
