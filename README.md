## Fortnox

This module offers integration with the official Fortnox API.

It relates drupal entities to Fortnox resources like Invoices, 
Customers, Suppliers etc.

For now, only the Supplier Invoice resource is integrated by default in the
module, but it can be easily extended for any Fortnox resource you need.

## Installation

The recommended way to install this module is by using composer to require 
it into your project.

`composer require drupal/fortnox`

## Configuration

The configuration page of this module can be found in the admin main navigation
under "Configuration -> Fortnox" or at the following path 
"/admin/config/fortnox".

#### Enable Integration

This module needs your Client-Secret key and the Integration API Code to work.
 
When registering to fortnox, you'll get a Client-Secret and a Client-ID. Copy 
and paste your Client-Secret in the form displayed on the Fortnox module's
configuration page in your Drupal site. 

To get the Integration API Code you have to add a new integration to your
Fortnox account on the Fortnox website. Log in with your Fortnox account,
in the right top corner click on your username to open the dropdown menu
and select "Manage users" link. It will open a new page and at the bottom of
it you'll have a list with integrations. Just above the integration list you
should see an "Add integration" button that will open a popup with one field
where you'll paste your Client-ID. Submit the form and your Integration API Code
should appear. Copy it, go to your Drupal site on the Fortnox configuration 
page and add the Integration API Code in the corresponding field. Submit the
form and a message to inform you if the integration was enabled or not should 
appear.

## How to use

By default, you can create/update Supplier Invoice entities from Drupal in your
Fortnox account. To do this go to the module's configuration page and add the 
API endpoint URL that can be found in the Fortnox [documentation](https://developer.fortnox.se/documentation/).

In the left menu under the RESOURCES category you can click the Supplier 
Invoices link. On the new page, under the POST section, copy the link from the
URL structure and paste it in the Drupal configuration page.

Next you can create a new Invoice entity from "Content -> Invoice list" or at
the "/admin/content/invoice". Check the "Create/Update in Fortnox" checkbox
and the entity will be created/updated in Fortnox too.

### Extend

To extend this module create a new custom entity that extends the 
FortnoxContentEntityBase class. By extending this class you'll get by default
the "Create/Update in Fortnox" checkbox and the entity will automatically
be created/updated in Fortnox if the field is checked.

The last thing you have to do is to create a new Resource plugin by extending
the ResourceBase class. In this plugin you have to map the values of your 
Drupal entity to the values of the Fortnox Resource by implementing the
"fieldMapping" and "processValues" abstract methods.

After creating the plugin, on the module's configuration page, a new field
to enter the endpoint API url for your new integration should be available.
Copy and test it from documentation, submit the form and you are ready to go. 

You can check the InvoiceResource plugin for a working example (also the 
Invoice entity).
