<?php

/**
 * @file
 * Contains credentials.page.inc.
 *
 * Page callback for Credentials entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Credentials templates.
 *
 * Default template: credentials.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_credentials(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
