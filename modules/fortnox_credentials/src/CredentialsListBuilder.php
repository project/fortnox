<?php

namespace Drupal\fortnox_credentials;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Credentials entities.
 *
 * @ingroup fortnox_credentials
 */
class CredentialsListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Credentials ID');
    $header['name'] = $this->t('Client Secret');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\fortnox_credentials\Entity\Credentials $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.credentials.edit_form',
      ['credentials' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
