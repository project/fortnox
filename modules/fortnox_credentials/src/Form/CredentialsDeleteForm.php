<?php

namespace Drupal\fortnox_credentials\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Url;

/**
 * Class CredentialsDeleteForm.
 */
class CredentialsDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    return Url::fromRoute('fortnox_credentials.add_or_view_credentials', ['user' => $this->currentUser()->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('fortnox_credentials.add_or_view_credentials', ['user' => $this->currentUser()->id()]);
  }

}
