<?php

namespace Drupal\fortnox_credentials\Controller;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\fortnox_credentials\Entity\CredentialsInterface;
use Drupal\user\UserInterface;

/**
 * Class UserLocalTask.
 */
class UserLocalTask extends ControllerBase {

  /**
   * Redirects user to add credentials form or to its existing credentials.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user parameter.
   *
   * @return array
   *   Returns the entity form or an empty array.
   */
  public function userLocalTaskRouting(UserInterface $user) {
    $currentUserId = $this->currentUser()->id();
    $build = [];
    try {
      $credentialsAdded = $this->entityTypeManager()->getStorage('credentials')->loadByProperties(['user_id' => $currentUserId]);
      if (!empty($credentialsAdded) && reset($credentialsAdded) instanceof CredentialsInterface) {
        $entity = reset($credentialsAdded);
        $build = $this->entityFormBuilder()->getForm($entity);
      }
      else {
        $credentials = $this->entityTypeManager()->getStorage('credentials')->create([]);
        $build = $this->entityFormBuilder()->getForm($credentials);
      }
    }
    catch (PluginException $e) {
      $this->loggerFactory->get('Fortnox Credentials')->error($e->getMessage());
    }

    return $build;
  }

}
