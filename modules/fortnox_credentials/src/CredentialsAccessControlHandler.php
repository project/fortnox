<?php

namespace Drupal\fortnox_credentials;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Credentials entity.
 *
 * @see \Drupal\fortnox_credentials\Entity\Credentials.
 */
class CredentialsAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\fortnox_credentials\Entity\CredentialsInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished credentials entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published credentials entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit credentials entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete credentials entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add credentials entities');
  }

}
