<?php

namespace Drupal\fortnox_credentials\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Credentials entities.
 *
 * @ingroup fortnox_credentials
 */
interface CredentialsInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Client Secret.
   *
   * @return string
   *   Returns the Client Secret of the integration.
   */
  public function getClientSecret();

  /**
   * Sets the Client Secret.
   *
   * @param string $clientSecret
   *   The Client Secret string.
   *
   * @return \Drupal\fortnox_credentials\Entity\CredentialsInterface
   *   The called Credentials entity.
   */
  public function setClientSecret($clientSecret);

  /**
   * Gets the API Code.
   *
   * @return string
   *   Returns the API Code string.
   */
  public function getApiCode();

  /**
   * Sets the API Code.
   *
   * @param string $apiCode
   *   Sets the API Code string.
   *
   * @return \Drupal\fortnox_credentials\Entity\CredentialsInterface
   *   The called Credentials entity.
   */
  public function setApiCode($apiCode);

  /**
   * Gets the Credentials creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Credentials.
   */
  public function getCreatedTime();

  /**
   * Sets the Credentials creation timestamp.
   *
   * @param int $timestamp
   *   The Credentials creation timestamp.
   *
   * @return \Drupal\fortnox_credentials\Entity\CredentialsInterface
   *   The called Credentials entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets access token from the entity.
   *
   * @return string
   *   Returns the access token string.
   */
  public function getAccessToken();

  /**
   * Sets an access token on the entity.
   *
   * @param string $accessToken
   *   Sets the access token string on the entity.
   *
   * @return \Drupal\fortnox_credentials\Entity\CredentialsInterface
   *   The called Credentials entity.
   */
  public function setAccessToken($accessToken);

}
