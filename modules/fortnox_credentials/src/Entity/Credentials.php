<?php

namespace Drupal\fortnox_credentials\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Credentials entity.
 *
 * @ingroup fortnox_credentials
 *
 * @ContentEntityType(
 *   id = "credentials",
 *   label = @Translation("Credentials"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\fortnox_credentials\CredentialsListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\fortnox_credentials\Form\CredentialsForm",
 *       "add" = "Drupal\fortnox_credentials\Form\CredentialsForm",
 *       "edit" = "Drupal\fortnox_credentials\Form\CredentialsForm",
 *       "delete" = "Drupal\fortnox_credentials\Form\CredentialsDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\fortnox_credentials\CredentialsHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\fortnox_credentials\CredentialsAccessControlHandler",
 *   },
 *   base_table = "credentials",
 *   translatable = FALSE,
 *   admin_permission = "administer credentials entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "client_secret",
 *     "api_code" = "api_code",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/credentials/{credentials}",
 *     "add-form" = "/admin/structure/credentials/add",
 *     "edit-form" = "/admin/structure/credentials/{credentials}/edit",
 *     "delete-form" = "/admin/structure/credentials/{credentials}/delete",
 *     "collection" = "/admin/structure/credentials",
 *   },
 *   field_ui_base_route = "credentials.settings"
 * )
 */
class Credentials extends ContentEntityBase implements CredentialsInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getClientSecret() {
    return $this->get('client_secret')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setClientSecret($clientSecret) {
    $this->set('client_secret', $clientSecret);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiCode() {
    return $this->get('api_code')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setApiCode($apiCode) {
    $this->set('api_code', $apiCode);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken() {
    return $this->get('access_token')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAccessToken($accessToken) {
    $this->set('access_token', $accessToken);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Credentials entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['client_secret'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Client Secret'))
      ->setDescription(t('The Client Secret of the integration.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['api_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('API Code'))
      ->setDescription(t('The API Code of the integration.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['access_token'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Access Token'))
      ->setDescription(t('The access token used when making requests.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setReadOnly(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Credentials is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
